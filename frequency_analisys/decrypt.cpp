#include "decrypt.h"

Decryption::Decryption() {
    preparedStatMono = readStatMono();
    inputStatMono = zeroArray();
}

std::array<std::pair<QChar, double>, 26> Decryption::readStatMono(const QString &fname) {
    QFile file(fname);
    std::array<std::pair<QChar, double>, 26> st;
    size_t i = 0;

    if ( !file.open(QIODevice::ReadOnly | QIODevice::Text) ) {
        return zeroArray();
    }

    while ( !file.atEnd() ) {
        QString str = file.readLine();
        QStringList lst = str.split(" ");
        st[i].first = (lst[0])[0];
        st[i].second = lst[1].toDouble();
        i++;
}

    std::sort(st.begin(), st.end(), [](const std::pair<QChar, double> &a, const std::pair<QChar, double> &b) {
        return a.second > b.second;
    });

    file.close();
    return st;
}

std::array<std::pair<QChar, double>, 26> Decryption::zeroArray() {
    std::array<std::pair<QChar, double>, 26> zeros = {{{'a', 0}, {'b', 0}, {'c', 0}, {'d', 0},
                                                         {'e', 0}, {'f', 0}, {'g', 0}, {'h', 0},
                                                         {'i', 0}, {'j', 0}, {'k', 0}, {'l', 0},
                                                         {'m', 0}, {'n', 0}, {'o', 0}, {'p', 0},
                                                         {'q', 0}, {'r', 0}, {'s', 0}, {'t', 0},
                                                         {'u', 0}, {'v', 0}, {'w', 0}, {'x', 0},
                                                         {'y', 0}, {'z', 0}}};
    return zeros;
}

//std::array<std::pair<QString, int>, 676> Decryption::readStatBi(const QString &fname) {
//    QFile file(fname);
//    std::array<std::pair<QString, int>, 676> st;
//    size_t i = 0;
//    if ( file.open(QIODevice::ReadOnly | QIODevice::Text) ) {
//        while ( !file.atEnd() ) {
//            QString str = file.readLine();
//            QStringList lst = str.split(" ");
//            st[i].first = lst[0];
//            st[i].second = lst[1].toInt();
//            i++;
//        }
//    }

//    file.close();
//    for (auto i : st) {
//        qDebug() << i << " ";
//    }
//    return st;
//}

void Decryption::decryptMonograms(QString &txt) {
    txt = txt.toLower();

    for (auto &&i : txt) {
        if (i.isLetter()) {
            inputStatMono[i.toLatin1() - 'a'].second++;
        }
    }

    std::sort(inputStatMono.begin(), inputStatMono.end(), [](const std::pair<QChar, int> &a, const std::pair<QChar, int> &b){
        return a.second > b.second;
    });

    for (size_t i = 0; i != inputStatMono.size(); ++i) {
        decryptMono.emplace(inputStatMono[i].first, preparedStatMono[i].first);
    }
    qDebug() << decryptMono;

    for (auto i = txt.begin(); i != txt.end(); ++i) {
        if ( i->isLetter() ) {
            txt.replace(std::distance(txt.begin(), i), 1, decryptMono[*i]);
        }
    }

   return;
}

//QString Decryption::decryptBigrams(QString &txt) {
//    txt = txt.toLower();
//    std::set<QString> bi;

//    for (int i = 0; i < txt.size() - 1; ++i) {
//        if ( txt[i].isLetter() && txt[i+1].isLetter() ) {
//            bi.insert(txt.mid(i, 2));
//        }
//    }

//    return txt;
//}

void Decryption::swapLetters(const QChar &a, const QChar &b, QString &txt) {
    for (int i = 0; i < txt.size(); ++i) {
        if (txt[i] == a) {
            txt.replace(i, 1, b);
        }

        else if (txt[i] == b) {
            txt.replace(i, 1, a);
        }
    }

    return;
}
