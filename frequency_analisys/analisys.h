#ifndef ANALISYS_H
#define ANALISYS_H

#include <QDialog>

namespace Ui {
class Analisys;
}


class Analisys : public QDialog
{
    Q_OBJECT

public:
    explicit Analisys(QWidget *parent = nullptr);
    ~Analisys();

private slots:
    void on_pushButton_choose_file_clicked();
    void on_pushButton_decrypt_clicked();
    void on_pushButton_save_file_clicked();
    void on_pushButton_replace_clicked();

private:
    Ui::Analisys *ui;
};

#endif // ANALISYS_H
