#include "analisys.h"
#include "ui_analisys.h"
#include "decrypt.h"
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QTextStream>
#include <QDateTime>
#include <algorithm>

Analisys::Analisys(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Analisys)
{
    ui->setupUi(this);
}

Analisys::~Analisys()
{
    delete ui;
}

void Analisys::on_pushButton_choose_file_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this, "Open a file to decrypt", QDir::homePath(), "Text files (*.txt)");
    ui->label_filename->setText(fname);

    QFile fl(fname);
    if ( !fl.open(QFile::ReadOnly | QIODevice::Text) ) {
        QMessageBox::warning(this, "title", "Failed to open file");
        return;
    }

    QTextStream rfile(&fl);
    QString text = rfile.readAll();
    fl.close();
    ui->textEdit_before->setHtml(text);
}

void Analisys::on_pushButton_decrypt_clicked()
{
    QString text = ui->textEdit_before->toPlainText();

    if (text == "") {
        QMessageBox::warning(this, "title", "File is not chosen");
        return;
    }

    Decryption dec;
    dec.decryptMonograms(text);

    ui->textEdit_after->setHtml(text);

}

void Analisys::on_pushButton_save_file_clicked()
{
    QString fname = QFileDialog::getSaveFileName(this, tr("Save"), "", "Text files (*.txt)");
    QString text = ui->textEdit_after->toPlainText();

    QFile fl(fname);

    if ( fl.open(QIODevice::WriteOnly | QIODevice::Text) ) {
        QTextStream stream(&fl);
        stream << text;
        fl.close();
    }
}

void Analisys::on_pushButton_replace_clicked()
{
    bool checked = true;
    QString text = ui->textEdit_after->toPlainText();
    QChar c1 = (ui->comboBox_from->currentText())[0];
    QChar c2 = (ui->comboBox_to->currentText())[0];

    if (checked) {
        Decryption dec;
        dec.swapLetters(c1, c2, text);
        ui->textEdit_after->setHtml(text);
    }
}
