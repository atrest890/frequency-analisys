#include "authorization.h"
#include "ui_authorization.h"

Authorization::Authorization(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Authorization)
{
    ui->setupUi(this);
}

Authorization::~Authorization()
{
    delete ui;
}

void Authorization::connectionIsClosed()
{
    db.close();
    db.removeDatabase(QSqlDatabase::defaultConnection);
}

bool Authorization::connectionIsOpened()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/home/elisva/Документы/Учеба/database.db");

    if ( !db.open() )
    {
        qDebug() << db.lastError().text();
        return false;
    }

    qDebug() << "Connected...";
    return true;
}


void Authorization::on_pushButton_login_clicked()
{
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text();

    connectionIsOpened();
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username=:username AND password=:password");
    query.bindValue(":username", username);
    query.bindValue(":password", QString(QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Md5).toHex()));
    if ( query.exec() )
    {
        int cnt = 0;

        while ( query.next() )
            cnt++;

        if (cnt == 1)
        {
            ui->label_login_status->setText("Correct");
            connectionIsClosed();
            this->hide();
            Analisys analisys;
            analisys.setModal(true);
            analisys.exec();
        }

        else if (cnt < 1)
        {
            ui->label_login_status->setText("Username or password is not correct");
        }
    }

}

void Authorization::on_pushButton_signup_clicked()
{
    QString new_username = ui->lineEdit_new_username->text();
    QString new_password = ui->lineEdit_new_password->text();
    QString confirm_new_password = ui->lineEdit_confirm_new_password->text();

    connectionIsOpened();
    QSqlQuery query;
    query.prepare("SELECT 1 FROM users WHERE username=:username");
    query.bindValue(":username", new_username);

    if ( !query.exec() ) {
        qDebug() << "Something wrong with database: it is not connected";
        return;
    }

    int cnt = 0;
    while ( query.next() ) {
        cnt++;
    }

    if (cnt == 0) {
        if (new_password == confirm_new_password) {
            new_password = QString(QCryptographicHash::hash(new_password.toUtf8(), QCryptographicHash::Md5).toHex());
            query.prepare("INSERT INTO users(username, password) values(:username, :password)");
            query.bindValue(":username", new_username);
            query.bindValue(":password", new_password);
            query.exec();
            ui->label_signup_status->setText("You successfully signed up.");
        }
    }

    else {
        ui->label_signup_status->setText("This username already exists. Please choose another one");
    }
}

