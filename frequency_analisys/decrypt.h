#ifndef DECRYPT_H
#define DECRYPT_H

#include <array>
#include <map>
#include <set>
#include <QString>
#include <iterator>
#include <QChar>
#include <algorithm>
#include <QDebug>
#include <QFile>

class Decryption
{
private:
    std::array<std::pair<QChar, double>, 26> preparedStatMono;
    std::array<std::pair<QChar, double>, 26> inputStatMono;
    std::map<QChar, QChar> decryptMono;

private:
    std::array<std::pair<QChar, double>, 26> readStatMono(const QString &fname = "/home/elisva/Документы/monostat.txt");
    std::array<std::pair<QChar, double>, 26> zeroArray();

public:
    Decryption();
    void decryptMonograms(QString &);
    void swapLetters(const QChar &, const QChar &, QString &);
    ~Decryption() = default;
};

#endif // DECRYPT_H
