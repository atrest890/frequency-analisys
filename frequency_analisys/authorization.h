#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H
#include <QMainWindow>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include <QCryptographicHash>
#include <analisys.h>

namespace Ui {
class Authorization;
}

class Authorization : public QMainWindow
{
    Q_OBJECT

public:
    void connectionIsClosed();
    bool connectionIsOpened();

     QSqlDatabase db;

public:
    explicit Authorization(QWidget *parent = nullptr);
    ~Authorization();

private slots:
    void on_pushButton_login_clicked();
    void on_pushButton_signup_clicked();

private:
    Ui::Authorization *ui;
};

#endif // AUTHORIZATION_H
