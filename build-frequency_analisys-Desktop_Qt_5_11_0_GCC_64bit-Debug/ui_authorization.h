/********************************************************************************
** Form generated from reading UI file 'authorization.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHORIZATION_H
#define UI_AUTHORIZATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Authorization
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QLineEdit *lineEdit_username;
    QLineEdit *lineEdit_password;
    QLabel *label;
    QLabel *label_2;
    QGroupBox *groupBox_2;
    QLineEdit *lineEdit_new_username;
    QLineEdit *lineEdit_new_password;
    QLineEdit *lineEdit_confirm_new_password;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *pushButton_login;
    QPushButton *pushButton_signup;
    QLabel *label_status;
    QLabel *label_login_status;
    QLabel *label_signup_status;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Authorization)
    {
        if (Authorization->objectName().isEmpty())
            Authorization->setObjectName(QStringLiteral("Authorization"));
        Authorization->resize(518, 642);
        centralWidget = new QWidget(Authorization);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 20, 481, 171));
        lineEdit_username = new QLineEdit(groupBox);
        lineEdit_username->setObjectName(QStringLiteral("lineEdit_username"));
        lineEdit_username->setGeometry(QRect(160, 30, 201, 25));
        lineEdit_password = new QLineEdit(groupBox);
        lineEdit_password->setObjectName(QStringLiteral("lineEdit_password"));
        lineEdit_password->setGeometry(QRect(160, 100, 201, 25));
        lineEdit_password->setEchoMode(QLineEdit::Password);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(26, 30, 91, 20));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 100, 111, 17));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 270, 481, 231));
        lineEdit_new_username = new QLineEdit(groupBox_2);
        lineEdit_new_username->setObjectName(QStringLiteral("lineEdit_new_username"));
        lineEdit_new_username->setGeometry(QRect(150, 70, 201, 25));
        lineEdit_new_password = new QLineEdit(groupBox_2);
        lineEdit_new_password->setObjectName(QStringLiteral("lineEdit_new_password"));
        lineEdit_new_password->setGeometry(QRect(150, 120, 201, 25));
        lineEdit_new_password->setEchoMode(QLineEdit::Password);
        lineEdit_confirm_new_password = new QLineEdit(groupBox_2);
        lineEdit_confirm_new_password->setObjectName(QStringLiteral("lineEdit_confirm_new_password"));
        lineEdit_confirm_new_password->setGeometry(QRect(150, 170, 201, 25));
        lineEdit_confirm_new_password->setEchoMode(QLineEdit::Password);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 70, 91, 20));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(40, 120, 111, 17));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 170, 131, 17));
        pushButton_login = new QPushButton(centralWidget);
        pushButton_login->setObjectName(QStringLiteral("pushButton_login"));
        pushButton_login->setGeometry(QRect(210, 200, 89, 25));
        pushButton_signup = new QPushButton(centralWidget);
        pushButton_signup->setObjectName(QStringLiteral("pushButton_signup"));
        pushButton_signup->setGeometry(QRect(220, 510, 89, 25));
        label_status = new QLabel(centralWidget);
        label_status->setObjectName(QStringLiteral("label_status"));
        label_status->setGeometry(QRect(20, 610, 481, 17));
        label_login_status = new QLabel(centralWidget);
        label_login_status->setObjectName(QStringLiteral("label_login_status"));
        label_login_status->setGeometry(QRect(20, 250, 481, 17));
        label_signup_status = new QLabel(centralWidget);
        label_signup_status->setObjectName(QStringLiteral("label_signup_status"));
        label_signup_status->setGeometry(QRect(20, 560, 481, 17));
        Authorization->setCentralWidget(centralWidget);
        groupBox->raise();
        groupBox_2->raise();
        label_status->raise();
        label_login_status->raise();
        label_signup_status->raise();
        pushButton_login->raise();
        pushButton_signup->raise();
        statusBar = new QStatusBar(Authorization);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Authorization->setStatusBar(statusBar);

        retranslateUi(Authorization);

        QMetaObject::connectSlotsByName(Authorization);
    } // setupUi

    void retranslateUi(QMainWindow *Authorization)
    {
        Authorization->setWindowTitle(QApplication::translate("Authorization", "Authorization", nullptr));
        groupBox->setTitle(QApplication::translate("Authorization", "Log in", nullptr));
        label->setText(QApplication::translate("Authorization", "username", nullptr));
        label_2->setText(QApplication::translate("Authorization", "password", nullptr));
        groupBox_2->setTitle(QApplication::translate("Authorization", "Sign up", nullptr));
        label_3->setText(QApplication::translate("Authorization", "username", nullptr));
        label_4->setText(QApplication::translate("Authorization", "password", nullptr));
        label_5->setText(QApplication::translate("Authorization", "confirm password", nullptr));
        pushButton_login->setText(QApplication::translate("Authorization", "Ok", nullptr));
        pushButton_signup->setText(QApplication::translate("Authorization", "Ok", nullptr));
        label_status->setText(QString());
        label_login_status->setText(QString());
        label_signup_status->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Authorization: public Ui_Authorization {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHORIZATION_H
