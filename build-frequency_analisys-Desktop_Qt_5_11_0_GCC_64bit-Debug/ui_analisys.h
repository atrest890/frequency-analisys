/********************************************************************************
** Form generated from reading UI file 'analisys.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ANALISYS_H
#define UI_ANALISYS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_Analisys
{
public:
    QPushButton *pushButton_choose_file;
    QPushButton *pushButton_decrypt;
    QLabel *label_filename;
    QRadioButton *radioButton_monograms;
    QRadioButton *radioButton_bigrams;
    QRadioButton *radioButton_trigrams;

    void setupUi(QDialog *Analisys)
    {
        if (Analisys->objectName().isEmpty())
            Analisys->setObjectName(QStringLiteral("Analisys"));
        Analisys->resize(447, 201);
        pushButton_choose_file = new QPushButton(Analisys);
        pushButton_choose_file->setObjectName(QStringLiteral("pushButton_choose_file"));
        pushButton_choose_file->setGeometry(QRect(50, 50, 121, 25));
        pushButton_decrypt = new QPushButton(Analisys);
        pushButton_decrypt->setObjectName(QStringLiteral("pushButton_decrypt"));
        pushButton_decrypt->setGeometry(QRect(240, 50, 111, 25));
        label_filename = new QLabel(Analisys);
        label_filename->setObjectName(QStringLiteral("label_filename"));
        label_filename->setGeometry(QRect(50, 90, 121, 17));
        radioButton_monograms = new QRadioButton(Analisys);
        radioButton_monograms->setObjectName(QStringLiteral("radioButton_monograms"));
        radioButton_monograms->setGeometry(QRect(240, 90, 112, 23));
        radioButton_bigrams = new QRadioButton(Analisys);
        radioButton_bigrams->setObjectName(QStringLiteral("radioButton_bigrams"));
        radioButton_bigrams->setGeometry(QRect(240, 120, 112, 23));
        radioButton_trigrams = new QRadioButton(Analisys);
        radioButton_trigrams->setObjectName(QStringLiteral("radioButton_trigrams"));
        radioButton_trigrams->setGeometry(QRect(240, 150, 112, 23));

        retranslateUi(Analisys);

        QMetaObject::connectSlotsByName(Analisys);
    } // setupUi

    void retranslateUi(QDialog *Analisys)
    {
        Analisys->setWindowTitle(QApplication::translate("Analisys", "Dialog", nullptr));
        pushButton_choose_file->setText(QApplication::translate("Analisys", "Choose the file", nullptr));
        pushButton_decrypt->setText(QApplication::translate("Analisys", "Decrypt with", nullptr));
        label_filename->setText(QString());
        radioButton_monograms->setText(QApplication::translate("Analisys", "monograms", nullptr));
        radioButton_bigrams->setText(QApplication::translate("Analisys", "bigrams", nullptr));
        radioButton_trigrams->setText(QApplication::translate("Analisys", "trigrams", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Analisys: public Ui_Analisys {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ANALISYS_H
