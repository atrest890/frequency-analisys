#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <set>
#include <iterator>

int main()
{
	std::ifstream in("/home/elisva/text_trans.txt");
	std::ofstream out("/home/elisva/text.txt");
	std::string text;
	std::set<std::string> bigrams;
	//std::vector<std::string> bigrams;
	//std::getline(std::cin, text);
	//std::transform(text.begin(), text.end(), text.begin(), [](unsigned char c) { return std::tolower(c); } );
	
	while (in >> text)
	{
		std::transform(text.begin(), text.end(), text.begin(), ::tolower); 

		for (std::string::size_type i = 0; i != text.size() - 1; ++i)
			bigrams.insert(text.substr(i, 2));
	}
	copy(bigrams.begin(), bigrams.end(), std::ostream_iterator<std::string>(out, " "));
	out.close();

	return 0;
}
